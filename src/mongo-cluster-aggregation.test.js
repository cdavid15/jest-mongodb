const { MongoClient } = require("mongodb");
const data = require("./data/cluster-data-input.json");
const aggregation = require("./query/cluster-aggregation.json");

describe("'Cluster' Aggregation Tests", () => {
  let connection, db, col;

  beforeAll(async () => {
    connection = await MongoClient.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = await connection.db();
    col = db.collection("cluster");
    await col.insertMany(data);
  });

  afterAll(async () => {
    await col.deleteMany({});
    await connection.close();
  });

  it("verify aggregation cluster counts", async () => {
    col
      .aggregate(aggregation)
      .toArray()
      .then(results => {
        expect(results[0].total).toEqual(5);
        expect(results[1].total).toEqual(5);
        expect(results[2].total).toEqual(43);
        expect(results[3].total).toEqual(50);
        expect(results[4].total).toEqual(20);
      });
  });

  it("verify aggregation output", async () => {
    const expectedOutput = require("./data/cluster-expected-output.json");
    const results = await col.aggregate(aggregation).toArray();

    expect(results).toEqual(expectedOutput);
  });
});

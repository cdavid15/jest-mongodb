const { MongoClient } = require("mongodb");
const data = require("./data/files-input.json");
const aggregationQuery = require("./query/files-aggregation.json");

describe("Files input and aggregations", () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = await connection.db();
  });

  afterAll(async () => {
    await connection.close();
  });

  it("should aggregate docs from collection", async () => {
    const files = db.collection("files");

    await files.insertMany(data);

    const topFiles = await files.aggregate(aggregationQuery).toArray();

    expect(topFiles).toEqual([
      expect.objectContaining({ count: 3 }),
      //   { _id: "Document", count: 3 },
      { _id: "Image", count: 2 },
      { _id: "Video", count: 1 }
    ]);

    const expectedOutput = require("./data/files-expected-output.json");
    expect(topFiles).toEqual(expectedOutput);
  });
});
